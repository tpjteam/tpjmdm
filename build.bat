rem @echo off
rem ------ 
rem 2060922: lance de build directement sur le fichier en argument
rem issu de build.bat de cocoon 

:: == en principe doit etre defini en ENVAR TO ADAPTER AU POSTE
:: TODO en l'absence de definition ENVAR, ADAPTER les lignes suivantes

IF NOT "%PROG_HOME%" == "" (
	echo %PROG_HOME%
	) else (
		echo ENVAR PROG_HOME set to default
		set PROG_HOME=D:\Progs
		)

IF NOT "%JAVA_HOME%" == "" (
	echo  %JAVA_HOME%
	) else (
		echo ENVAR JAVA_HOME set to default
		set JAVA_HOME=D:\Progs\jdk1.5.0_08
		)

IF NOT "%ANT_HOME%" == "" (
	echo  %ANT_HOME%
	) else (
		echo ENVAR ANT_HOME set to default
		set ANT_HOME=%PROG_HOME%\Ant165
		)

:: ==================================

pause
if not "%LIB_HOME%" == "" goto skip
set LIB_HOME=lib
:skip


set LOCALCLASSPATH=%_JAVA_HOME%\lib\tools.jar
for %%i in (%LIB_HOME%\*.jar) do call tools\lcp.bat %%i


echo.
echo Building with classpath %LOCALCLASSPATH%

echo.
echo Starting Ant...


set JAVACMD=%JAVA_HOME%\bin\java.exe
set ANT_ARGS=
set ANT_OPTS=
:: "-logger org.apache.tools.ant.NoBannerLogger"
set ANT_CMD_LINE_ARGS=

:runAntWithClasspath
:: appel via Java dans les anciennes cocoon qui appellent direct java
:: "%JAVACMD%" %ANT_OPTS% -classpath "%ANT_HOME%\lib\ant-launcher.jar" "-Dant.home=%ANT_HOME%" org.apache.tools.ant.launch.Launcher %ANT_ARGS% -cp "%LOCALCLASSPATH%" %ANT_CMD_LINE_ARGS%
:: on appelle plutot le ant.bat integre
call %ANT_HOME%\bin\ant %ANT_OPTS% %1 %2 %3 %4 %5 %6 %7 %8 %9


goto end

:error

echo "ERROR: JAVA_HOME not found in your environment."
echo.
echo "Please, set the JAVA_HOME variable in your environment to match the"
echo "location of the Java Virtual Machine you want to use."



:end
pause

set LOCALCLASSPATH=
:: set ANT_HOME=
set LIB_HOME=
